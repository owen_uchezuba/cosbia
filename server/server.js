const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const app = express();

require('dotenv').config();
const mongoose = require('mongoose');
const schema = require('./api/schema');

const db_url= process.env.db_url

const db_connect = mongoose.connect(db_url,{useUnifiedTopology:true,useNewUrlParser:true})
const cors = require('cors')

app.use(cors({origin:'http://localhost:4200'}))

app.use('/api',graphqlHTTP({
    schema:schema,
    graphiql:true
}))


app.listen(process.env.PORT, ()=>{
    console.log('Listening to port ', process.env.PORT)
})