import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChildService } from '../child.service';
import {interval,timer} from 'rxjs'
import { CountdownComponent } from 'ngx-countdown'


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

@ViewChild('cd', { static: false }) private countdown: CountdownComponent;



  level:any
  count:number=0
  time
  selectedvalue:string
  buttonvalue:string="Next"


  

  constructor(private router:Router, private route:ActivatedRoute,public child:ChildService) { 
    
  }

  home(){
    this.router.navigate(['/child/home'])
  }
submit(){  
  //console.log(this.child.questions.length)
  if(this.count < this.child.questions.length){
    this.child.correctanswer+=(this.selectedvalue === this.child.questions[this.count].answer)?1:0;
    this.count++
    this.buttonvalue=(this.count === (this.child.questions.length-1))?"Finish":"Next";
    console.log(this.child.correctanswer)
    this.selectedvalue=""
    if(this.count===this.child.questions.length)
        this.router.navigate(['child/result'])
  }

}

exit(){
  this.child.correctanswer = 0;
  this.router.navigate(['/child/result'])
}

  startTime(){        
    this.child.list(this.level).subscribe(({data})=>{
      console.log(data.quiz_query)
      this.child.questions = data.quiz_query
      console.log(this.child.questions)
      //this.startTime();
    })
  }

  eventHandler(event){
      //console.log(event)
      if(event.action=="done" && event.text=="00:00:00"){
        this.router.navigate(['/child/result'])
      }
  }

  ngOnInit(): void {
    this.time = this.child.display_time()
    this.level = this.route.snapshot.params['platform']
    console.log('you are in ',this.level)
    this.startTime();

  }

}
