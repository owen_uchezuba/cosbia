import { Component, OnInit } from '@angular/core';
import { ChildService } from '../child.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  levels:[]=[]

  constructor(private child:ChildService,private device:DeviceDetectorService) { }

  deviceinfo
  mobile
  tablet
  desktop

  play(level){
    //this.child.

  }
  ngOnInit(): void {
    this.child.all_levels().subscribe(({data})=>{
      this.levels = data.all_levels
      console.log(this.levels)
    })

    this.deviceinfo = this.device.getDeviceInfo()
    const mobile = this.device.isMobile()
    const tablet = this.device.isTablet()
    const desktop = this.device.isDesktop()
    this.mobile = this.device.isMobile()
    this.tablet = this.device.isTablet()
    this.desktop =this.device.isDesktop()

    console.log(this.deviceinfo,'\n',mobile,'\n',tablet,'\n',desktop)
  }

}
