import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag'

@Injectable({
  providedIn: 'root'
})
export class ChildService {

  constructor(private apollo:Apollo) { }

  public seconds:number
  public questions:any[]=[]
  public timer
  public correctanswer:number=0;


  reg = gql`
    mutation register($username:String,$email:String,$password:String){
          register(username:$username,email:$email,password:$password)
    }
  `

  public registering(username,email,password){
    return this.apollo.mutate<any>({
      mutation:this.reg,
      variables:{
        username,email,password
      }
    })
  }

  lev = gql`
    query checklevel($username:String){
          checklevel(username:username)
    }
  `

  public level(username:string){
      return this.apollo.watchQuery<any>({
        query:this.lev,
        variables:{
            username
        }
      }).valueChanges
  }


  all_lev = gql`
    query userlevel($username:String){
      userlevel(username:$username){
        level,
        score
      }
    }
  `

  public levels(username:string){
    return this.apollo.watchQuery<any>({
      query:this.all_lev,
      variables:{
        username
      }
    }).valueChanges
  }


  private q = gql`
    mutation addquiz($quiz:String,$questions:String,
      $op1:String,$op2:String,
      $op3:String,$op4:String,
      $answer:String,$decrip:String,){

        addquiz(quiz:$quiz,questions:$questions,
          op1:$op1,op2:$op2,
          op3:$op3,op4:$op4,
          answer:$answer,decrip:$decrip,)
      }
  `

  public quizz(quiz,questions,op1,op2,op3,op4,answer,decrip){
    return this.apollo.mutate<any>({
      mutation:this.q,
      variables:{
        quiz,questions,op1,op2,op3,op4,answer,decrip
      }
    })
  }

  private c = gql`
    mutation addlevel($level:String,$description:String){
              addlevel(level:$level,description:$description)
    }
  `
  public addlevel(level,description){
    return this.apollo.mutate<any>({
      mutation:this.c,
      variables:{
         level,description
      }
    })
  }




all = gql`
  query all_levels{
    all_levels{
      level,
      description
    }
  }
`
public all_levels(){
  return this.apollo.watchQuery<any>({
    query: this.all
  }).valueChanges
}


private quiz_query = gql`
    query quiz_query($level:String){
      quiz_query(level:$level){
        question,
        option1,
        option2,
        option3,
        option4,
        answer,
        decrip
      }
    }
`

public list(level){
  return this.apollo.watchQuery<any>({
    query:this.quiz_query,
    variables:{
      level
    }
  }).valueChanges
}

public display_time(){
  return Math.floor(this.seconds/3600)+":"
  +Math.floor(this.seconds/60)+":"
  +Math.floor(this.seconds%60)
}



}
