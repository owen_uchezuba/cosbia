import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ChildService } from '../child.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  emailpattern ="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
 // passpattern = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
  constructor(private route:Router,private child:ChildService) { }

  submit(form:NgForm){
    console.log('form',form)
    this.child.registering(form.value.username,form.value.email,form.value.password)
    .subscribe(({data})=>{
      console.log(data)
      localStorage.setItem('name',data.register)
      this.route.navigate(['/child/home'])
    })

  }

  ngOnInit(): void {
  }

}
