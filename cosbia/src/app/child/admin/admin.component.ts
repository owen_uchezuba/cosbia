import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ChildService } from '../child.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private child:ChildService) { }

  submit(form:NgForm){
      this.child.addlevel(form.value.name,form.value.des)
      .subscribe((data)=>{
        console.log('hello',data)
      })
  }

  submitquiz(form:NgForm){
    // form.value.op1 = (form.value.op1== "")? "null": form.value.op1
    // form.value.op2 = (form.value.op2== "")? "null": form.value.op2
    // form.value.op3 = (form.value.op3== "")? "null": form.value.op3
    // form.value.op4 = (form.value.op4== "")? "null": form.value.op4
    console.log('value',form.value.op1)
      this.child.quizz(form.value.name,form.value.que,form.value.op1,form.value.op2,form.value.op3,form.value.op4,form.value.ans,form.value.des)
      .subscribe((data)=>{
        console.log('Saved',data)
      })
  }

  ngOnInit(): void {
  }

}
