import { Component, OnInit } from '@angular/core';
import { ChildService } from '../child.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  constructor(public child:ChildService,private router:Router) { }
  percentage:number=0
  
  home(){
    this.child.correctanswer = 0;
    this.router.navigate(['/child/home'])
  }

  ngOnInit(): void {
    this.percentage =  Math.round((this.child.correctanswer/this.child.questions.length)*100)  ;
    console.log('loc',localStorage.getItem('name'))
  }

}
