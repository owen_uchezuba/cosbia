import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit(): void {
    // if(localStorage.getItem('name')!=null){
    //   this.route.navigate(['/child/home/'])
    // }
  }

}
