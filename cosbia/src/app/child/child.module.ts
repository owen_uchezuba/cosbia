import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildRoutingModule } from './child-routing.module';
import { ChildComponent } from './child.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import { ResultComponent } from './result/result.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { from } from 'rxjs';
import { AdminComponent } from './admin/admin.component';
import {CountdownModule} from 'ngx-countdown'
import {DeviceDetectorModule} from 'ngx-device-detector'
import {MatRadioModule} from '@angular/material/radio';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [ChildComponent, RegisterComponent, HomeComponent, QuizComponent, ResultComponent, AdminComponent, AboutComponent, ContactComponent],
  imports: [
    CommonModule,
    ChildRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CountdownModule,
    DeviceDetectorModule,
    MatRadioModule
  ],
  exports:[
    ChildComponent
  ]
})
export class ChildModule { }
