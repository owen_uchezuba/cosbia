//importScripts('./ngsw-worker.js')
importScripts('./ngsw-worker.js')

self.addEventListener('sync',(event)=>{
    if(event.tag === 'post-data'){
        //method you want to be background sync
        event.waitUntil(addData)
    }
})

function addData(){
    let obj = {
        name: "isaac"
    }
    fetch('//server url',{
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(obj)
    }).then(()=>Promise.resolve()).catch(()=>Promise.reject())
}